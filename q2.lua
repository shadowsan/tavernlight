function printSmallGuildNames(memberCount)
    -- this method is supposed to print names of all guilds that have less than memberCount max members
    -- if we have max_members column in table, as I understood the base server does not have a logic with max_members
    local selectGuildQuery = "SELECT `name` FROM `guilds` WHERE `max_members` < %d;"
    local query = db.storeQuery(string.format(selectGuildQuery, memberCount))
    if not query then
        return
    end

    repeat
        local guildName = result.getString(query, "name")
        print(guildName)
    until not result.next(query)
    result.free(query)
end
