// private
void Game::addItemToPlayerInternal(Player *player, uint16_t itemId) {
  Item *item = Item::CreateItem(itemId);
  if (!item) {
    return;
  }
  auto ret = g_game.internalAddItem(player->getInbox(), item, INDEX_WHEREEVER,
                                    FLAG_NOLIMIT);
  if (ret != RETURNVALUE_NOERROR) {
    delete item;
  }
  if (player->isOffline()) {
    IOLoginData::savePlayer(player);
  }
}

void Game::addItemToPlayer(const std::string &recipient, uint16_t itemId) {
  Player *player = g_game.getPlayerByName(recipient);
  if (player) {
    addItemToPlayerInternal(player, itemId);
    return;
  }

  Player temporaryPlayer(nullptr);
  if (!IOLoginData::loadPlayerByName(&temporaryPlayer, recipient)) {
    return;
  }
  addItemToPlayerInternal(&temporaryPlayer, itemId);
}