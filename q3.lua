function removeMemberFromPartyWithName(playerId, membername)
    local player = Player(playerId)
    if player == nil then
        return false
    end
    local party = player:getParty()
    if party == nil or party:getLeader() ~= player then -- only leaders can remove/kick party members
        return
    end

    for _, member in pairs(party:getMembers()) do
        if member:getName() == membername then
            party:removeMember(member)
        end
    end
end
